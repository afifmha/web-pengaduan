/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.1.36-MariaDB : Database - db_pemas
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_pemas` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_pemas`;

/*Table structure for table `masyarakat` */

DROP TABLE IF EXISTS `masyarakat`;

CREATE TABLE `masyarakat` (
  `nik` char(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`nik`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `masyarakat` */

insert  into `masyarakat`(`nik`,`id_user`,`nama`,`username`,`password`,`telp`,`created_at`,`updated_at`) values 
('3200321234567732',5,'Afif Maulana','afifm','$2y$10$M9i/AKsWhO2jdumf9eSMBONHU06o7JV1/wu3iOlu53liRLAFyG1xO','0813124152577','2020-04-21 08:21:25','2020-04-21 08:21:25');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2020_04_06_041223_create_masyarakat_table',1),
(2,'2020_04_06_041447_create_pengaduan_table',1),
(3,'2020_04_06_041502_create_petugas_table',1),
(4,'2020_04_06_041516_create_tanggapan_table',1),
(5,'2020_04_06_054307_create_users_table',2);

/*Table structure for table `pengaduan` */

DROP TABLE IF EXISTS `pengaduan`;

CREATE TABLE `pengaduan` (
  `id_pengaduan` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_pengaduan` date NOT NULL,
  `nik` char(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi_laporan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('0','proses','selesai') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_pengaduan`),
  KEY `FK_NIK` (`nik`),
  CONSTRAINT `FK_NIK` FOREIGN KEY (`nik`) REFERENCES `masyarakat` (`nik`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `pengaduan` */

insert  into `pengaduan`(`id_pengaduan`,`tgl_pengaduan`,`nik`,`isi_laporan`,`foto`,`status`,`created_at`,`updated_at`) values 
(14,'2020-04-21','3200321234567732','Renovasi gedung',NULL,'selesai',NULL,'2020-04-21 09:01:17');

/*Table structure for table `petugas` */

DROP TABLE IF EXISTS `petugas`;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` bigint(20) NOT NULL,
  `nama_petugas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` enum('admin','petugas') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `petugas` */

insert  into `petugas`(`id_petugas`,`id_user`,`nama_petugas`,`username`,`password`,`telp`,`level`,`created_at`,`updated_at`) values 
(1,1,'admin','admin','$2y$10$frm8/tbLV7UC1HV/dZgMh.mhU1qxeG3rLVe6RMZvWaHO.jxe1ochS','0','admin','2020-04-12 09:26:16','2020-04-12 09:26:16'),
(6,2,'petugas','petugas','$2y$10$JL.TZoFjEigR0Oy3KeCiKeP7Bn6H0mI2YB.McZ5WcIToXe6Cd5DCu','098723457654','petugas','2020-04-12 19:42:23','2020-04-12 19:42:23');

/*Table structure for table `tanggapan` */

DROP TABLE IF EXISTS `tanggapan`;

CREATE TABLE `tanggapan` (
  `id_tanggapan` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengaduan` int(11) NOT NULL,
  `tgl_tanggapan` date NOT NULL,
  `tanggapan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_tanggapan`),
  KEY `FK_PENGADUAN` (`id_pengaduan`),
  KEY `FK_PETUGAS` (`id_petugas`),
  CONSTRAINT `FK_PENGADUAN` FOREIGN KEY (`id_pengaduan`) REFERENCES `pengaduan` (`id_pengaduan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_PETUGAS` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `tanggapan` */

insert  into `tanggapan`(`id_tanggapan`,`id_pengaduan`,`tgl_tanggapan`,`tanggapan`,`id_petugas`,`created_at`,`updated_at`) values 
(5,14,'2020-04-21','Siap Laksanakan',1,'2020-04-21 09:01:17','2020-04-21 09:01:17');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` enum('admin','petugas','masyarakat') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`username`,`password`,`level`,`created_at`,`updated_at`) values 
(1,'admin','$2y$10$BIOy8wJ/.ykTuCnBOhbmsuUE08DGirmm2rn5XMVxra9HeD0YPp.Q6','admin','2020-04-12 09:26:16','2020-04-12 09:26:16'),
(2,'petugas','$2y$10$JL.TZoFjEigR0Oy3KeCiKeP7Bn6H0mI2YB.McZ5WcIToXe6Cd5DCu','petugas','2020-04-12 19:42:23','2020-04-12 19:42:23'),
(5,'afifm','$2y$10$M9i/AKsWhO2jdumf9eSMBONHU06o7JV1/wu3iOlu53liRLAFyG1xO','masyarakat','2020-04-21 08:21:25','2020-04-21 08:21:25');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
